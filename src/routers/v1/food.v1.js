const express = require("express");
const foodRouter = express.Router();

const createNewFood = require("../../controllers/Food/createNewFood");
foodRouter.post("/newFood", createNewFood());

const getAllFoodInfo = require("../../controllers/Food/getAllFoodInfo");
foodRouter.get("/allFood", getAllFoodInfo());

module.exports = foodRouter;
