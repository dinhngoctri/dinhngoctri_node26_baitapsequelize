const express = require("express");
const likeResRouter = express.Router();

const likeRes = require("../../controllers/LikeRes/likeRes");
likeResRouter.post("/like", likeRes());

const unlikeRes = require("../../controllers/LikeRes/unlikeRes");
likeResRouter.delete("/user-:userId/unlike/restaurant-:resId", unlikeRes());

const getAllResByUserLike = require("../../controllers/LikeRes/getAllResByUserLike");
likeResRouter.get("/restaurantInfo/:userId", getAllResByUserLike());

const getAllUserLikeByRes = require("../../controllers/LikeRes/getAllUserLikeByRes");
likeResRouter.get("/userInfo/:resId", getAllUserLikeByRes());

module.exports = likeResRouter;
