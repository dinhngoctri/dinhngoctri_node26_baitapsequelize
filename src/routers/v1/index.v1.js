const express = require("express");
const foodRouter = require("./food.v1");
const foodTypeRouter = require("./foodType.v1");
const likeResRouter = require("./likeRes.v1");
const orderRouter = require("./order.v1");
const rateResRouter = require("./rateRes.v1");
const userRouter = require("./user.v1");

const v1 = express.Router();
v1.use("/user", userRouter);
v1.use("/order", orderRouter);
v1.use("/food", foodRouter);
v1.use("/food", foodTypeRouter);
v1.use("/like", likeResRouter);
v1.use("/rateRes", rateResRouter);

module.exports = v1;
