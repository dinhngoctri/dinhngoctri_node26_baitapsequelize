const express = require("express");
const foodTypeRouter = express.Router();

const getAllFoodTypeInfo = require("../../controllers/FoodType/getAllFoodTypeInfo");
foodTypeRouter.get("/allFoodType", getAllFoodTypeInfo());

module.exports = foodTypeRouter;
