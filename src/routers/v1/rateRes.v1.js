const express = require("express");
const rateResRouter = express.Router();

const getAllResByUserRate = require("../../controllers/RateRes/getAllResByUserRate");
rateResRouter.get("/restaurantInfo/ratedBy-:userId", getAllResByUserRate());

const getAllUserRateByRes = require("../../controllers/RateRes/getAllUserRateByRes");
rateResRouter.get("/userRate/restaurant-:resId", getAllUserRateByRes());

const createRateRes = require("../../controllers/RateRes/createRateRes");
rateResRouter.post("/rateRestaurant", createRateRes());

module.exports = rateResRouter;
