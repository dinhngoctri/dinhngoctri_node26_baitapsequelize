const express = require("express");
const orderRouter = express.Router();

const createNewOrder = require("../../controllers/Order/createNewOrder");
orderRouter.post("/newOrder", createNewOrder());

module.exports = orderRouter;
