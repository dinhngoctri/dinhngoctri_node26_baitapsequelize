const express = require("express");
const userRouter = express.Router();

const getAllUserInfo = require("../../controllers/User/getAllUserInfo");
userRouter.get("/allUser", getAllUserInfo());

const createNewUser = require("../../controllers/User/createNewUser");
userRouter.post("/newUser", createNewUser());

module.exports = userRouter;
