const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "RateRes",
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "user_id",
      },

      resId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "res_id",
      },

      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },

      dataRate: {
        type: DataTypes.TIME,
        defaultValue: DataTypes.NOW,
        field: "date_rate",
      },
    },

    {
      tableName: "rate_res",
      timestamps: false,
    }
  );
};
