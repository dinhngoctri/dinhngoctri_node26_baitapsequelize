const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "LikeRes",
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "user_id",
      },

      resId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "res_id",
      },

      dateLike: {
        type: DataTypes.TIME,
        defaultValue: DataTypes.NOW,
        field: "date_like",
      },
    },

    {
      tableName: "like_res",
      timestamps: false,
    }
  );
};
