const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "SubFood",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: "sub_id",
      },

      subName: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "sub_name",
      },

      subPrice: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: "sub_price",
      },

      foodId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        // references: {
        //   model: Food,
        //   key: "food_id",
        // },
        field: "food_id",
      },
    },

    {
      tableName: "sub_food",
      timestamps: false,
    }
  );
};
