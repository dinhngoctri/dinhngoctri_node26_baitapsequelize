const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Food",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: "food_id",
      },

      foodName: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "food_name",
      },

      image: {
        type: DataTypes.STRING,
      },

      price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },

      desc: {
        type: DataTypes.STRING,
      },

      typeId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        // references: {
        //   model: FoodType,
        //   key: "type_id",
        // },
        field: "type_id",
      },
    },

    {
      tableName: "food",
      timestamps: false,
    }
  );
};
