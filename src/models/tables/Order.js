const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Order",
    {
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "user_id",
      },

      foodId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        field: "food_id",
      },

      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },

      code: {
        type: DataTypes.STRING,
      },

      arrSubId: {
        type: DataTypes.STRING,
        field: "arr_sub_id",
      },
    },

    {
      tableName: "order",
      timestamps: false,
    }
  );
};
