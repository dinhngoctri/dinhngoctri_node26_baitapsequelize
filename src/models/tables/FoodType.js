const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "FoodType",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: "type_id",
      },

      typeName: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "type_name",
      },
    },

    {
      tableName: "food_type",
      timestamps: false,
    }
  );
};
