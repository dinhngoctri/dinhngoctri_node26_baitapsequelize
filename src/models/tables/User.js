const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: "user_id",
      },

      fullName: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "full_name",
      },

      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },

      password: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "pass_word",
      },
    },

    {
      tableName: "user",
      timestamps: false,
    }
  );
};
