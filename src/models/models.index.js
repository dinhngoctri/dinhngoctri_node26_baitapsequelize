const { Sequelize } = require("sequelize");
const sequelize = new Sequelize("node26-foods", "root", "dinhtri2605", {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log(`SEQUELIZE CONNECTED`);
  } catch (error) {
    console.log(`UNABLE TO CONNECT`, error);
  }
})();
const {
  defineFoodRelationship,
  defineLikeRelationship,
  defineRateRelationship,
} = require("./models.relationship");

const Food = require("./tables/Food")(sequelize);
const FoodType = require("./tables/FoodType")(sequelize);
const LikeRes = require("./tables/LikeRes")(sequelize);
const Order = require("./tables/Order")(sequelize);
const RateRes = require("./tables/RateRes")(sequelize);
const Restaurant = require("./tables/Restaurant")(sequelize);
const SubFood = require("./tables/SubFood")(sequelize);
const User = require("./tables/User")(sequelize);

defineFoodRelationship(Food, FoodType);
defineLikeRelationship(LikeRes, Restaurant, User);
defineRateRelationship(RateRes, Restaurant, User);

module.exports = { Food, FoodType, LikeRes, Order, RateRes, Restaurant, SubFood, User };
