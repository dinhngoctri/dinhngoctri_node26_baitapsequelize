const defineFoodRelationship = (Food, FoodType) => {
  Food.belongsTo(FoodType, { foreignKey: "typeId" });
  FoodType.hasMany(Food, { foreignKey: "typeId" });
};

const defineLikeRelationship = (LikeRes, Restaurant, User) => {
  LikeRes.belongsTo(Restaurant, { foreignKey: "resId" });
  Restaurant.hasMany(LikeRes, { foreignKey: "resId" });

  LikeRes.belongsTo(User, { foreignKey: "userId" });
  User.hasMany(LikeRes, { foreignKey: "userId" });
  // User.belongsToMany(Restaurant, { through: LikeRes });
  // Restaurant.belongsToMany(User, { through: LikeRes });
};

const defineRateRelationship = (RateRes, Restaurant, User) => {
  RateRes.belongsTo(Restaurant, { foreignKey: "resId" });
  Restaurant.hasMany(RateRes, { foreignKey: "resId" });

  RateRes.belongsTo(User, { foreignKey: "userId" });
  User.hasMany(RateRes, { foreignKey: "userId" });
};

module.exports = {
  defineFoodRelationship,
  defineLikeRelationship,
  defineRateRelationship,
};
