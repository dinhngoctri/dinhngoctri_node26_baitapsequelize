const { response } = require("express");
const likeResGet = require("../../services/LikeRes/LikeRes.get.service");
const userGet = require("../../services/User/User.get.service");

const getAllResByUserLike = () => {
  return async (req, res) => {
    try {
      const { userId } = req.params;

      const userInfo = await userGet.getUserDetailInfo(userId);
      const data = await likeResGet.getAllResByUserLike(userId);

      const response =
        data.length <= 0
          ? "This user hasn't like yet"
          : data.map((item) => item.Restaurant);

      res.status(200).json({
        data: {
          user: userInfo.fullName,
          restaurant: response,
        },
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllResByUserLike;
