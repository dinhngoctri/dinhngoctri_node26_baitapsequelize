const likeResPost = require("../../services/LikeRes/LikeRes.post.service");

const likeRes = () => {
  return async (req, res) => {
    try {
      const data = await likeResPost.likeRes(req.body);
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = likeRes;
