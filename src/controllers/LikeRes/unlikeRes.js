const likeResDelete = require("../../services/LikeRes/LikeRes.delete.service");

const unlikeRes = () => {
  return async (req, res) => {
    try {
      const { userId, resId } = req.params;
      await likeResDelete.unlikeRes(userId, resId);
      res.status(200).json({ data: "success" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };
};

module.exports = unlikeRes;
