const likeResGet = require("../../services/LikeRes/LikeRes.get.service");
const restaurantGet = require("../../services/Restaurant/Restaurant.get.service");

const getAllUserLikeByRes = () => {
  return async (req, res) => {
    try {
      const { resId } = req.params;

      const restaurant = await restaurantGet.getRestaurantDetailInfo(resId);
      const data = await likeResGet.getAllUserLikeByRes(resId);

      const response =
        data.length <= 0
          ? "This restaurant has none like"
          : data.map((item) => item.User);

      res.status(200).json({
        data: {
          restaurant: restaurant.resName,
          userLiked: response,
        },
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllUserLikeByRes;
