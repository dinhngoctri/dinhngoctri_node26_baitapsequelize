const userGet = require("../../services/User/User.get.service");

const getAllUserInfo = () => {
  return async (req, res) => {
    try {
      const data = await userGet.getAllUserInfo();
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllUserInfo;
