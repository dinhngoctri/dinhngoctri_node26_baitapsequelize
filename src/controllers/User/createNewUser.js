const userPost = require("../../services/User/User.post.service");

const createNewUser = () => {
  return async (req, res) => {
    try {
      const data = await userPost.createNewUser(req.body);
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = createNewUser;
