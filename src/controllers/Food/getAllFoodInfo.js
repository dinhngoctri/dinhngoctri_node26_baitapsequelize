const foodGet = require("../../services/Food/Food.get.service");

const getAllFoodInfo = () => {
  return async (req, res) => {
    try {
      const data = await foodGet.getAllFoodInfo();
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllFoodInfo;
