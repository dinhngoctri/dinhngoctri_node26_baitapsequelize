const foodPost = require("../../services/Food/Food.post.service");

const createNewFood = () => {
  return async (req, res) => {
    try {
      const data = await foodPost.createNewFood(req.body);
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = createNewFood;
