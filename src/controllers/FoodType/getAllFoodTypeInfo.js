const foodTypeGet = require("../../services/FoodType/FoodType.get.service");

const getAllFoodTypeInfo = () => {
  return async (req, res) => {
    try {
      const data = await foodTypeGet.getAllFoodTypeInfo();
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllFoodTypeInfo;
