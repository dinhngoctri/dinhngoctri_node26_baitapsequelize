const rateResGet = require("../../services/RateRes/RateRes.get.service");
const userGet = require("../../services/User/User.get.service");

const getAllResByUserLike = () => {
  return async (req, res) => {
    try {
      const { userId } = req.params;

      const userInfo = await userGet.getUserDetailInfo(userId);
      const data = await rateResGet.getAllResByUserRate(userId);

      const response =
        data.length <= 0
          ? "This user hasn't rate yet"
          : data.map((item) => item.Restaurant);

      res.status(200).json({
        data: {
          user: userInfo.fullName,
          restaurant: response,
        },
      });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = getAllResByUserLike;
