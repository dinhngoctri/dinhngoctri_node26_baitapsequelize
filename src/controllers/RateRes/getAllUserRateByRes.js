const rateResGet = require("../../services/RateRes/RateRes.get.service");
const restaurantGet = require("../../services/Restaurant/Restaurant.get.service");

const getAllResByUserLike = () => {
  return async (req, res) => {
    try {
      const { resId } = req.params;

      const resInfo = await restaurantGet.getRestaurantDetailInfo(resId);
      const data = await rateResGet.getAllUserRateByRes(resId);

      const response =
        data.length <= 0
          ? "This restaurant hasn't be rated yet"
          : data.map((item) => item.User);

      res.status(200).json({
        data: {
          restaurant: resInfo.resName,
          userRated: response,
        },
      });
    } catch (error) {
      res.status(500).json({ error, he: req.params.resId });
    }
  };
};

module.exports = getAllResByUserLike;
