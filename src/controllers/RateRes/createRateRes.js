const rateResPost = require("../../services/RateRes/RateRes.post.service");

const createRateRes = () => {
  return async (req, res) => {
    try {
      const data = await rateResPost.createRateRes(req.body);
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = createRateRes;
