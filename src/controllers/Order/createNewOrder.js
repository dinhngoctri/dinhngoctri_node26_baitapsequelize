const orderPost = require("../../services/Order/Order.post.service");

const createNewOrder = () => {
  return async (req, res) => {
    try {
      const data = await orderPost.createNewOrder(req.body);
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ error });
    }
  };
};

module.exports = createNewOrder;
