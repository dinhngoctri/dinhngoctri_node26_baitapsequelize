const { Restaurant } = require("../../models/models.index");

class GetRestaurant {
  async getRestaurantDetailInfo(resId) {
    try {
      const result = await Restaurant.findOne({ where: { resId } });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const restaurantGet = new GetRestaurant();
module.exports = restaurantGet;
