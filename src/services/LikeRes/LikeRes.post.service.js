const { LikeRes } = require("../../models/models.index");

class PostLikeRes {
  async likeRes(likeInfo) {
    try {
      const result = await LikeRes.create(likeInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const likeResPost = new PostLikeRes();
module.exports = likeResPost;
