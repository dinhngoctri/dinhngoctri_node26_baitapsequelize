const { LikeRes } = require("../../models/models.index");

class DeleteLikeRes {
  async unlikeRes(userId, resId) {
    try {
      const isExist = await LikeRes.findOne({ where: { userId, resId } });
      if (!isExist) {
        throw new Error("already unlike");
      }
      const result = await LikeRes.destroy({ where: { userId, resId } });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const likeResDelete = new DeleteLikeRes();
module.exports = likeResDelete;
