const { LikeRes, Restaurant, User } = require("../../models/models.index");

class GetLikeRes {
  async getAllResByUserLike(userId) {
    try {
      const result = await LikeRes.findAll({ where: { userId }, include: Restaurant });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getAllUserLikeByRes(resId) {
    try {
      const result = await LikeRes.findAll({ where: { resId }, include: User });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const likeResGet = new GetLikeRes();
module.exports = likeResGet;
