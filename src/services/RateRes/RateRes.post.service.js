const { RateRes } = require("../../models/models.index");

class PostRateRes {
  async createRateRes(rateInfo) {
    try {
      const result = await RateRes.create(rateInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const rateResPost = new PostRateRes();
module.exports = rateResPost;
