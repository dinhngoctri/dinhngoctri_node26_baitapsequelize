const { RateRes, Restaurant, User } = require("../../models/models.index");

class GetRateRes {
  async getAllResByUserRate(userId) {
    try {
      const result = await RateRes.findAll({ where: { userId }, include: Restaurant });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getAllUserRateByRes(resId) {
    try {
      const result = await RateRes.findAll({ where: { resId }, include: User });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const rateResGet = new GetRateRes();
module.exports = rateResGet;
