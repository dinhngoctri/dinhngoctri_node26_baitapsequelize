const { Food, FoodType } = require("../../models/models.index");

class GetFoodType {
  async getAllFoodTypeInfo() {
    try {
      const result = await FoodType.findAll({ include: Food });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const foodTypeGet = new GetFoodType();
module.exports = foodTypeGet;
