const { Order } = require("../../models/models.index");

class PostOrder {
  async createNewOrder(orderInfo) {
    try {
      const result = await Order.create(orderInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const orderPost = new PostOrder();
module.exports = orderPost;
