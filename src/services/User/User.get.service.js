const { User } = require("../../models/models.index");

class GetUser {
  async getAllUserInfo() {
    try {
      const result = await User.findAll();
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getUserDetailInfo(userId) {
    try {
      const result = await User.findOne({ where: { id: userId } });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const userGet = new GetUser();
module.exports = userGet;
