const { User } = require("../../models/models.index");

class PostUser {
  async createNewUser(newUser) {
    try {
      const result = await User.create(newUser);
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const userPost = new PostUser();
module.exports = userPost;
