const { Food } = require("../../models/models.index");

class PostFood {
  async createNewFood(foodInfo) {
    try {
      const result = await Food.create(foodInfo);
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const foodPost = new PostFood();
module.exports = foodPost;
