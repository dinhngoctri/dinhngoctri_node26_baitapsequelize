const { Food, FoodType } = require("../../models/models.index");

class GetFood {
  async getAllFoodInfo() {
    try {
      const result = await Food.findAll({ include: FoodType });
      return result;
    } catch (error) {
      throw error;
    }
  }
}

const foodGet = new GetFood();
module.exports = foodGet;
